package day02

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func RunDay() {
	parsedInput := loadInput()
	boxChecksum := generateBoxChecksum(parsedInput)

	commonIdChars := getTargetBoxId(parsedInput)

	fmt.Printf("Day 2.1: boxChecksum: %v\n", boxChecksum)
	fmt.Printf("Day 2.2: boxIdDiff: %v\n", commonIdChars)

}

func getTargetBoxId(parsedInput []string) string {
	foundValue := ""
	for _, i := range parsedInput {
		for _, c := range parsedInput {
			if i == c {
				continue
			}
			s := checkCompareBoxIds(i, c)
			if s != "" {
				foundValue = s
			}
		}
	}
	return foundValue
}
func checkCompareBoxIds(b1, b2 string) string {
	result := ""
	diff := 0
	for i, s1 := range b1 {
		if string(s1) == string(b2[i]) {
			result = result + string(s1)
		} else {
			diff++
		}
		if diff > 1 {
			return ""
		}
	}
	return result
}

func generateBoxChecksum(parsedInput []string) int {
	var twoCount, threeCount int

	for _, box := range parsedInput {
		two, three := checkBoxId(box)
		if two {
			twoCount += 1
		}
		if three {
			threeCount += 1
		}
	}
	return (twoCount * threeCount)
}

func checkBoxId(boxId string) (bool, bool) {
	charMap := make(map[rune]bool)
	two, three := false, false
	for _, c := range boxId {
		if !charMap[c] {
			charCount := strings.Count(boxId, string(c))
			switch charCount {
			case 2:
				two = true
			case 3:
				three = true
			}
			charMap[c] = true
		}

	}
	return two, three
}

func loadInput() []string {
	var parsedInput []string
	file, err := os.Open("day02/puzzle_input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		rawText := scanner.Text()
		parsedInput = append(parsedInput, rawText)
	}
	return parsedInput
}
