package day02

import (
	"testing"
)

func Test_generateBoxChecksum(t *testing.T) {
	tests := []struct {
		name        string
		parsedInput []string
		want        int
	}{
		{"sample Input", []string{
			"abcdef",
			"bababc",
			"abbcde",
			"abcccd",
			"aabcdd",
			"abcdee",
			"ababab",
		}, 12},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := generateBoxChecksum(tt.parsedInput); got != tt.want {
				t.Errorf("generateBoxChecksum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getTargetBoxId(t *testing.T) {
	tests := []struct {
		name        string
		parsedInput []string
		want        string
	}{
		{"sample Input", []string{
			"abcde",
			"fghij",
			"klmno",
			"pqrst",
			"fguij",
			"axcye",
			"wvxyz",
		}, "fgij"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getTargetBoxId(tt.parsedInput); got != tt.want {
				t.Errorf("getTargetBoxId() = %v, want %v", got, tt.want)
			}
		})
	}
}
