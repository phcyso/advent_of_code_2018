package day01

import (
	"testing"
)

type testValues struct {
	name          string
	parsedInput   []int
	expectedTotal int
	expectedDup   int
}

var happyTests = []testValues{
	{"expect 4,4", []int{2, 2, 2, -2}, 4, 4},
	{"expect 3,2", []int{2, -1, 1, 1}, 3, 2},
	{"expect 0,0", []int{+1, -1}, 0, 0},
	{"expect 4,10", []int{3, 3, 4, -2, -4}, 4, 10},
}

func Test_getTotal(t *testing.T) {
	for _, tt := range happyTests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getTotal(tt.parsedInput); got != tt.expectedTotal {
				t.Errorf("getTotal() = %v, want %v", got, tt.expectedTotal)
			}
		})
	}
}

func Test_getDuplicate(t *testing.T) {
	for _, tt := range happyTests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getDuplicate(tt.parsedInput); got != tt.expectedDup {
				t.Errorf("getDuplicate() = %v, want %v", got, tt.expectedDup)
			}
		})
	}
}
