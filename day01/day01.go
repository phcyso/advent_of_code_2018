package day01

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func RunDay() {
	parsedInput := loadInput()
	total := getTotal(parsedInput)
	firstDuplicate := getDuplicate(parsedInput)

	fmt.Printf("Day 1.1: Ending Frequency: %v\n", total)
	fmt.Printf("Day 1.2: First Duplicate: %v\n", firstDuplicate)

}

func getTotal(parsedInput []int) int {
	var returnValue int
	for _, f := range parsedInput {
		returnValue += f
	}
	return returnValue
}

func getDuplicate(parsedInput []int) int {
	foundValueMap := make(map[int]bool)
	foundValue := false
	currentTotal := 0
	foundValueMap[0] = true
	fmt.Println("Starting get dup check")
	for foundValue != true {
		for _, f := range parsedInput {
			currentTotal += f
			if foundValueMap[currentTotal] == true {
				foundValue = true
				return currentTotal
			}
			foundValueMap[currentTotal] = true

		}
	}
	return 0
}

func loadInput() []int {
	var parsedInput []int
	file, err := os.Open("day01/puzzle_input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		rawText := scanner.Text()
		parsedValue, _ := strconv.Atoi(rawText)
		parsedInput = append(parsedInput, parsedValue)
	}
	return parsedInput
}
