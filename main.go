package main

import (
	"fmt"

	"gitlab.com/phcyso/advent_of_code/day01"
	"gitlab.com/phcyso/advent_of_code/day02"
)

func main() {
	fmt.Println("-------------")
	day01.RunDay()
	fmt.Println("-------------")
	day02.RunDay()
}
